FROM python:3.9

ARG DEBIAN_FRONTEND=noninteractive

ENV LANG C.UTF-8

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

CMD ["uvicorn", "main:app", "--reload", "--host=0.0.0.0", "--port=8000"]
