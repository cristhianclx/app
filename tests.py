# -*- coding: utf-8 -*-

from fastapi.testclient import TestClient

from main import app


_client = TestClient(app)


class TestMain:
    def test_root(self):
        response = _client.get("/")
        assert response.status_code == 200
        data = response.json()
        assert data["message"] == "Nothing to see here"

    def test_ping(self):
        response = _client.get("/ping/")
        assert response.status_code == 200
        data = response.json()
        assert data["message"] == "pong"
