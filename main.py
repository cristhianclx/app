# -*- coding: utf-8 -*-

from os import getenv as os__getenv

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware


app: FastAPI = FastAPI(
    debug=True,
    title="API",
    redoc_url=None,
    version="v1",
)

# CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# /
@app.get("/", include_in_schema=False)
async def root():
    """
    /
    """
    return {
        "message": "Nothing to see here",
    }


# /ping/
@app.get("/ping/", summary="ping")
async def ping():
    """
    ping status for system, to know everything is working OK
    """
    return {
        "message": "pong",
        "v": os__getenv("v", "?"),
    }
